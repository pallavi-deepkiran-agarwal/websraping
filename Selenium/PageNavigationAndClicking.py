from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait


## learn about how to click on links, buttons and going to pages
PATH = "C:\Program Files (x86)\chromedriver.exe"
driver =webdriver.Chrome(PATH)

driver.get("https://www.techwithtim.net")

link = driver.find_element_by_link_text("Python Programming")
link.click()

try:
    element = WebDriverWait(driver,10).until(EC.presence_of_element_located((By.LINK_TEXT,"Beginner Python Tutorials")))
    element.click()

    get_started = WebDriverWait(driver,10).until(EC.presence_of_element_located((By.ID,"sow-button-19310003")))
    get_started.click()

    #to go back > driver.back()
    #to go forward > driver.forward()


finally:
    pass
