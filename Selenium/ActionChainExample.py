from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains


'''
ActionChains are a way to automate low level interactions 
such as mouse movements, mouse button actions, key press, 
and context menu interactions. 

This is useful for doing more complex actions like hover over and drag and drop.
'''
PATH = "C:\Program Files (x86)\chromedriver.exe"

driver = webdriver.Chrome(PATH)
driver.get("https://orteil.dashnet.org/cookieclicker/")

driver.implicitly_wait(5)

cookie = driver.find_element_by_id("bigCookie")
cookie_count = driver.find_element_by_id("cookies")

# acreating action object
action = ActionChains(driver)
action.click(cookie)
items = [driver.find_element_by_id("productPrice" + str(i)) for i in range(1,-1,-1)]

for i in range(500):
    action.perform()
    count = int(cookie_count.text.split(" ")[0])
    for item in items:
        value = int(item.text)
        if value < count:
            upgrade_action= ActionChains(driver)
            upgrade_action.move_to_element(item)
            upgrade_action.click()
            upgrade_action.perform()
