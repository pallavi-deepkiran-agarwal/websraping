from selenium import webdriver
from selenium.webdriver.common.keys import  Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

PATH = 'C:\Program Files (x86)\chromedriver.exe'
driver = webdriver.Chrome(PATH)

driver.get("https://data-flair.training/blogs/identifiers-in-python/")

element = WebDriverWait(driver,10).until(EC.presence_of_element_located((By.CLASS_NAME,"dfacc-lsbul")))
headers = element.find_elements_by_class_name("dfacc-lsbli")
for ele in headers:
    print(ele.text)

