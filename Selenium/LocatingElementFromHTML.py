from selenium import webdriver
from selenium.webdriver.common.keys import  Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

PATH = "C:\Program Files (x86)\chromedriver.exe"

driver = webdriver.Chrome(PATH)

driver.get("https://www.techwithtim.net")
_name_of_website = driver.title

print(f"Name of the website we are dealing with : {_name_of_website}")

search = driver.find_element_by_name("s")
search.send_keys("test")
search.send_keys(Keys.RETURN)


try:
    main = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "main")))
    articles = main.find_elements_by_tag_name("article")

    for article in articles:
        header = article.find_element_by_class_name("entry-summary")

        print(header.text,"\n")
except:
    print("error found")






## Extra notes:
## print(driver.page_source)  // this return the page's entire source code