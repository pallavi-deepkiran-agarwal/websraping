from selenium import webdriver
from selenium.webdriver.common.keys import  Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from orchestrator import features

PATH = 'C:\Program Files (x86)\chromedriver.exe'
driver = webdriver.Chrome(PATH)

driver.get("https://www.justkidsnmoms.com/moms-corner/")
features.scroll_down(driver)

try:
    element = WebDriverWait(driver,20).until(EC.presence_of_element_located((By.CLASS_NAME,"fusion-posts-container")))
    articles = element.find_elements_by_tag_name("article")
    for article in articles:
        header = article.find_element_by_tag_name("h2")
        link = header.find_element_by_tag_name("a").get_attribute("href")
        print(header.text,",\t",link)
except:
    print("found error")



