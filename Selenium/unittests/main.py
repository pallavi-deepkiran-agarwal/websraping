import unittest
from selenium import webdriver
import page


class PythonOrgSearch(unittest.TestCase):

    '''
    For each test case setup method is called ones
    '''
    def setUp(self):
        self.driver = webdriver.Chrome("C:\Program Files (x86)\chromedriver.exe")
        self.driver.get("http://www.python.org")

    def test_example(self):
        print("hello")
        assert True

    '''
    For each test case teardown method is called ones
    '''
    def tearDown(self):
        self.driver.close()

    def test_title(self):
        mainPage = page.MainPage(self.driver)
        assert mainPage.is_title_matches()

    def test_search_python(self):
        mainPage = page.MainPage(self.driver)
        mainPage.search_text_element = "pycon"
        mainPage.click_go_button()
        search_result_page = page.SearchResultPage(self.driver)
        assert search_result_page.is_result_found()


if __name__ == "__main__":
    unittest.main()