

import urllib.request
import urllib.error
from bs4 import BeautifulSoup
import ssl
import re
import json
import os
import shutil


ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE


if os.path.isdir("./InstagramData"):
    shutil.rmtree("./InstagramData")

folder = os.getcwd()
folder = os.path.join(folder, r'InstagramData')
os.makedirs(folder)

with open('user.txt') as f:
    userurls = f.readlines()

userurls = [x.strip() for x in userurls]
print(userurls)
for userurl in userurls:
    print(type(userurl))
    print(userurl)
    html = urllib.request.urlopen(userurl, context=ctx).read()
    soup = BeautifulSoup(html, 'html.parser')

    links = soup.find_all('script')
    usertitle = soup.find('title')

    user_folder_name = re.findall('(.+)\s\(@', usertitle.text)
    user_folder_name = str(user_folder_name[0])

    userfolderpath = os.path.join(folder, user_folder_name)
    os.makedirs(userfolderpath)

    for i in links:
        if "window._sharedData =" in i.text:
            y = re.findall("^window._sharedData = (.*);", i.text)

            y_str = y[0]
            a = json.loads(y_str)
            # print(type(a))

            url_list = a["entry_data"]["ProfilePage"][0]["graphql"]["user"]["edge_owner_to_timeline_media"]["edges"]
            # print(type(url_list))

            for key, item in enumerate(url_list):
                # print(item["node"]["display_url"])
                file = userfolderpath+"/"+str(key)+".jpg"
                url = item["node"]["display_url"]
                img_data = urllib.request.urlopen(url).read()
            # print(type(img_data))
                with open(file, 'wb') as handler:
                    handler.write(img_data)
















